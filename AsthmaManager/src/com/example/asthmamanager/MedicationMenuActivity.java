package com.example.asthmamanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MedicationMenuActivity extends AbstractActivity {
	private boolean resumeHasRun;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medication_menu_layout);
        setUpButtons();
    }
    @Override
    protected void onResume() {
    	super.onResume();
        /*if (!resumeHasRun) {
            resumeHasRun = true;
            return;
        }*/
        setUpButtons();
    }
    private void setUpButtons() {
    	SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        int nMedications = settings.getInt("nMedications", 0);
        
        Button newMedication = (Button) findViewById(R.id.new_medication);
        newMedication.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent newMedicationIntent = new Intent(view.getContext(), NewMedicationActivity.class);
        		startActivity(newMedicationIntent);
        	}
        });
        
        Button firstMedication = (Button) findViewById(R.id.med_1);
    	firstMedication.setText(settings.getString("medicationName1", 
    			"First Medication"));
    	final String medType1 = settings.getString("medicationType1", " ");
    	showAppropriateIcons(medType1, R.id.med_1);
    	firstMedication.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			startMedActivity(view, medType1);
    		}
    	});
        
        Button secondMedication = (Button) findViewById(R.id.med_2);
    	secondMedication.setText(settings.getString("medicationName2", 
    			"Second Medication"));
    	final String medType2 = settings.getString("medicationType2", "");
    	showAppropriateIcons(medType2, R.id.med_2);
    	secondMedication.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			startMedActivity(view, medType2);
    		}
    	});
       
    	Button thirdMedication = (Button) findViewById(R.id.med_3);
    	thirdMedication.setText(settings.getString("medicationName3", 
    			"Third Medication"));
    	final String medType3 = settings.getString("medicationType3", "");
    	showAppropriateIcons(medType3, R.id.med_3);
    	thirdMedication.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			startMedActivity(view, medType3);
    		}
    	});
        
    	Button fourthMedication = (Button) findViewById(R.id.med_4);
    	fourthMedication.setText(settings.getString("medicationName4", 
    			"Fourth Medication"));
    	final String medType4 = settings.getString("medicationType4", "");
    	showAppropriateIcons(medType4, R.id.med_4);
    	fourthMedication.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			startMedActivity(view, medType4);
    		}
    	});
    	
    	Button fifthMedication = (Button) findViewById(R.id.med_5);
    	fifthMedication.setText(settings.getString("medicationName5", 
    			"Fifth Medication"));
    	final String medType5 = settings.getString("medicationType5", "");
    	showAppropriateIcons(medType5, R.id.med_5);
    	fifthMedication.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			startMedActivity(view, medType5);
    		}
    	});
    	
    	if (nMedications < 1) {
    		firstMedication.setVisibility(View.GONE);
    	}
    	else {
    		firstMedication.setVisibility(View.VISIBLE);
    	}
    	if (nMedications < 2) {
    		secondMedication.setVisibility(View.GONE);
    	}
    	else {
    		secondMedication.setVisibility(View.VISIBLE);
    	}
    	if (nMedications < 3) {
    		thirdMedication.setVisibility(View.GONE);
    	}
    	else {
    		thirdMedication.setVisibility(View.VISIBLE);
    	}
    	if (nMedications < 4) {
    		fourthMedication.setVisibility(View.GONE);
    	}
    	else {
    		fourthMedication.setVisibility(View.VISIBLE);
    	}
    	if (nMedications < 5) {
    		fifthMedication.setVisibility(View.GONE);
    	}
    	else {
    		newMedication.setVisibility(View.GONE);
    		fifthMedication.setVisibility(View.VISIBLE);
    	}
    }
    
    private void startMedActivity(View view, String medicineType){
		if (medicineType.equals("rescue0")){			
			Intent rescueMedicationIntent = new Intent(view.getContext(), RescueMedicationActivity.class);
			startActivity(rescueMedicationIntent);
		}
		else if (medicineType.equals("daily0")){
			Intent firstDailyMedicationIntent = new Intent(view.getContext(), FirstDailyMedicationActivity.class);
			startActivity(firstDailyMedicationIntent);
		}
		else if (medicineType.equals("daily1")){
			Intent secondDailyMedicationIntent = new Intent(view.getContext(), SecondDailyMedicationActivity.class);
			startActivity(secondDailyMedicationIntent);
		}
		else if (medicineType.equals("nightly0")){
			Intent  firstNightlyMedicationIntent = new Intent(view.getContext(), FirstNightlyMedicationActivity.class);
			startActivity(firstNightlyMedicationIntent);
		}
		else if (medicineType.equals("nightly1")){
			Intent secondNightlyMedicationIntent = new Intent(view.getContext(), SecondNightlyMedicationActivity.class);
			startActivity(secondNightlyMedicationIntent);
		}
    }

    private void showAppropriateIcons(String medicineType, int medID){
    	TextView tv = (TextView) findViewById(medID);
    	if (medicineType.contains("rescue")){
        	tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.rescue, 0, 0, 0);
    	} else if (medicineType.contains("daily")){
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.sun, 0, 0, 0);
    	} else if (medicineType.contains("nightly")){
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.moon, 0, 0, 0);
    	}
    
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

}
