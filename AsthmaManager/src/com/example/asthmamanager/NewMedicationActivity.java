package com.example.asthmamanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class NewMedicationActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_medication);
        
        // Declare each button and set up its behavior.
        Button saveNewMedication = (Button) findViewById(R.id.save);
        saveNewMedication.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		SharedPreferences settings = 
        				getSharedPreferences("GLOBAL_SETTINGS", 0);
        		int nMedications = settings.getInt("nMedications", 0);
        		
        		EditText medNameField = (EditText) findViewById(R.id.medication_name);
        		EditText firstUseDateField = (EditText) findViewById(R.id.first_use_date);
        		EditText expirationDateField = (EditText) findViewById(R.id.expiration_date);
        		
        		String medName = medNameField.getText().toString();
        		String firstUseDate = firstUseDateField.getText().toString();
        		String expirationDate = expirationDateField.getText().toString();
        		
        		if (!checkLegalUserInput(medName, firstUseDate, expirationDate)){
        			return;
        		}
        		
        	    RadioButton rbDaily = (RadioButton) findViewById(R.id.daily);
        	    RadioButton rbNightly = (RadioButton) findViewById(R.id.nightly);
        	    RadioButton rbRescue = (RadioButton) findViewById(R.id.rescue);
        	    
        	    if(!isRadioButtonClicked(rbDaily, rbNightly, rbRescue)){
        	    	return;
        	    }
        	    
        	    SharedPreferences.Editor editor = settings.edit();
        	    saveRadioButtonInformation(rbDaily, rbNightly, rbRescue);
        		
        		editor.putString("medicationName" + (nMedications + 1), medName);
        		editor.putString("firstUseDate" + (nMedications + 1), firstUseDate);
        		editor.putString("expirationDate" + (nMedications + 1), expirationDate);
            	editor.putInt("nMedications", nMedications + 1);
        		
        		
        		editor.commit();
        		finish();
        	}
        });

    }

    private boolean isRadioButtonClicked(RadioButton rbDaily,
			RadioButton rbNightly, RadioButton rbRescue) {
	    if (!rbDaily.isChecked() && !rbNightly.isChecked() && !rbRescue.isChecked()){
    		CharSequence text = "Sorry, you need to choose a medicine type. \nPlease try again.";
    		createToast(text);
    		finish();
    		return false;
	    }
	    return true;
		
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        SharedPreferences settings = 
				getSharedPreferences("GLOBAL_SETTINGS", 0);
        SharedPreferences.Editor editor = settings.edit();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.daily:
                if (checked){
                	int dailyMedicationCount = settings.getInt("dailyMedicationCount", 0);
                	if (dailyMedicationCount > 1) {
                		CharSequence text = "Sorry, you can't have more than two daily medications.";
                		createToast(text);
                		return;
                	}
                }
                break;
            case R.id.nightly:
                if (checked){
                	int nightlyMedicationCount = settings.getInt("nightlyMedicationCount", 0);
                	if (nightlyMedicationCount > 1) {
                		CharSequence text = "Sorry, you can't have more than two nightly medications.";
                		createToast(text);
                		return;
                	}
                }
                break;
            case R.id.rescue:
                if (checked){
                	int rescueMedicationCount = settings.getInt("rescueMedicationCount", 0);
                	if (rescueMedicationCount == 1) {
                		CharSequence text = "Sorry, you can't have more than one rescue medication.";
                		createToast(text);
                		return;
                	}
                }
                break;
        }
        
        editor.commit();
    }
    
    private void createToast(CharSequence text){
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
    }  
    
    private boolean checkLegalUserInput(String medName, String firstUseDate, String expirationDate){
  		boolean isValidMedName = medName.matches("[a-zA-Z0-9 ]+");
	    boolean isValidFirstUseDate = firstUseDate.matches("[0-9]{2}-[0-9]{2}-[0-9]{4}");
	    boolean isValidExpirationDate = expirationDate.matches("[0-9]{2}-[0-9]{2}-[0-9]{4}");
	    
	    if(!isValidMedName || !isValidFirstUseDate || !isValidExpirationDate){
        	if(!isValidMedName){
        		//	have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the medicine name you entered had characters other than letters, numbers, or spaces. \nPlease try again.";
        		createToast(text);
        	}
        	if(!isValidFirstUseDate){
        		//	have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the first use date you entered does not match the intended formatting. \nPlease try again.";
        		createToast(text);
        	}
        	if(!isValidExpirationDate){
        		//	have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the expiration date you entered does not match the intended formatting. \nPlease try again.";
        		createToast(text);
        	}
    		finish();
    		return false;
	    	
	    }
	    return true;
    }
    
    
    private void saveRadioButtonInformation(RadioButton rbDaily, RadioButton rbNightly, RadioButton rbRescue){
    	SharedPreferences settings = 
				getSharedPreferences("GLOBAL_SETTINGS", 0);
		int nMedications = settings.getInt("nMedications", 0);
    	SharedPreferences.Editor editor = settings.edit();
	    if (rbDaily.isChecked()){
	    	int dailyMedicationCount = settings.getInt("dailyMedicationCount", 0);
    		editor.putString("medicationType" + (nMedications + 1), "daily" + dailyMedicationCount);
    		editor.putInt("numberIDofDailyMed" + (dailyMedicationCount + 1), nMedications + 1);
      		editor.putInt("dailyMedicationCount", (dailyMedicationCount + 1));
	    }
	    else if (rbNightly.isChecked()){
	    	int nightlyMedicationCount = settings.getInt("nightlyMedicationCount", 0);
        	editor.putString("medicationType" + (nMedications + 1), "nightly" + nightlyMedicationCount);
        	editor.putInt("numberIDofNightlyMed" + (nightlyMedicationCount + 1), nMedications + 1);
    		editor.putInt("nightlyMedicationCount", (nightlyMedicationCount + 1));
	    }
	    else if (rbRescue.isChecked()){
	    	int rescueMedicationCount = settings.getInt("rescueMedicationCount", 0);
        	editor.putString("medicationType" + (nMedications + 1), "rescue" + rescueMedicationCount);
        	editor.putInt("numberIDofRescueMed" + (rescueMedicationCount + 1), nMedications + 1);
    		editor.putInt("rescueMedicationCount", (rescueMedicationCount + 1));
	    }
	    editor.commit();
    }

}
