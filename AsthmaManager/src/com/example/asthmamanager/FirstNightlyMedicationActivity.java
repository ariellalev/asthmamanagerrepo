package com.example.asthmamanager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class FirstNightlyMedicationActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_nightly_med);
        
        SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        int id = settings.getInt("numberIDofNightlyMed" + 1, 0);
        
        TextView medTypeField = (TextView) findViewById(R.id.nightly_first_med_type);        
        String type = settings.getString("medicationType" + id, null);        
        medTypeField.setText("Medication Type: " + type.substring(0, type.length() - 1));
        
        TextView medNameField = (TextView) findViewById(R.id.nightly_first_med_name);
        medNameField.setText("Medication Name: " + settings.getString("medicationName" + id, null));
        
        TextView startDate = (TextView) findViewById(R.id.nightly_first_first_use_date);
        startDate.setText("Date of First Use: " + settings.getString("firstUseDate" + id, null));
        
        TextView expDate = (TextView) findViewById(R.id.nightly_first_expiration_date);
        expDate.setText("Expiration Date: " + settings.getString("expirationDate" + id, null));        

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    

	
}
