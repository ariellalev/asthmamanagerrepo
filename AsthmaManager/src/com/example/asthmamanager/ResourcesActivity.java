package com.example.asthmamanager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;


public class ResourcesActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resources);
        
        Button chop = (Button) findViewById(R.id.chop_link);
        chop.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Uri uri = Uri.parse("http://www.chop.edu/");
        		Intent chopIntent = new Intent(Intent.ACTION_VIEW, uri);
        		startActivity(chopIntent);
        	}
        });
       
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

}
