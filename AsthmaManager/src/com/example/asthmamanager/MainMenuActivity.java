package com.example.asthmamanager;


import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.net.Uri;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainMenuActivity extends AbstractActivity {

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_layout);
        
        // Declare each button and set up its behavior.
        
        Button medication = (Button) findViewById(R.id.main_menu_medication);
        medication.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent medicationIntent = new Intent(view.getContext(), MedicationMenuActivity.class);
        		startActivity(medicationIntent);
        	}
        });
        
        Button symptoms = (Button) findViewById(R.id.main_menu_symptoms);
        symptoms.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent symptomsIntent = new Intent(view.getContext(), 
						SymptomsMenuActivity.class);
				startActivity(symptomsIntent);
			}
		});
        
        Button triggerBusters = (Button) findViewById(R.id.main_menu_trigger_busters);
        triggerBusters.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				// DOESN'T DO ANYTHING RIGHT NOW cause there's no triggerbusters
			}
		});
        
        Button emergencyContact = (Button) findViewById(R.id.main_menu_emergency_contact);
        emergencyContact.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent emergencyContactIntent = new Intent(view.getContext(), 
						EmergencyContactMenuActivity.class);
				startActivity(emergencyContactIntent);
			}
		});
        
        Button resources = (Button) findViewById(R.id.main_menu_resources);
        resources.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent resourcesIntent = new Intent(view.getContext(), 
						ResourcesActivity.class);
				startActivity(resourcesIntent);
			}
		});
        checkMedicationDates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    private void checkMedicationDates() {
    	TimeZone tz = TimeZone.getTimeZone("America/New_York");
    	Calendar today = Calendar.getInstance(tz);
    	
    	SharedPreferences settings = 
				getSharedPreferences("GLOBAL_SETTINGS", 0);
    	int nMedications = settings.getInt("nMedications", 0);
    	String phoneNumber = settings.getString("emergencyContactNumber", "");
    	String message = "Sorry, you need to renew a medication";
    	
    	for (int i = 1; i <= nMedications; i++){
    		String expDate = settings.getString("expirationDate" + i, "");
    		int eMonth = Integer.parseInt(expDate.substring(0, 2));
    		int eDay = Integer.parseInt(expDate.substring(3, 5));
    		int eYear = Integer.parseInt(expDate.substring(6, 10));

    		Calendar expirationDateCalendar = Calendar.getInstance();
    		expirationDateCalendar.set(eYear, eMonth - 1, eDay);
    		
    		long daysBetween = daysBetween(today, expirationDateCalendar);
    		System.out.println(daysBetween);
    		if (daysBetween < 3) {
    			sendSMS(phoneNumber, message);
    		}
    	
    	}
    }
    
    private void sendSMS(String phoneNumber, String message) {        
		try {
			 Intent sendIntent = new Intent(Intent.ACTION_VIEW);
		     sendIntent.putExtra("sms_body", message); 
		     sendIntent.putExtra("address", phoneNumber);
		     sendIntent.setData(Uri.parse("smsto:" + phoneNumber));
		     sendIntent.setType("vnd.android-dir/mms-sms");
		     startActivity(sendIntent);

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
				"SMS faild, please try again later!",
				Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}    
    }
    
	@SuppressLint("NewApi")
	private static long daysBetween(Calendar start, Calendar finish){
		return TimeUnit.MILLISECONDS.toDays(
				finish.getTimeInMillis() - start.getTimeInMillis()
				);
	}
}
