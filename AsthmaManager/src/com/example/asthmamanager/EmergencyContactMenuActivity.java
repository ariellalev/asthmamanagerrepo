package com.example.asthmamanager;

import android.content.Context;
import android.content.Intent;
import android.content.ActivityNotFoundException;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class EmergencyContactMenuActivity extends AbstractActivity {
	private boolean resumeHasRun = false;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergency_contact_menu_layout);
        
     // Declare each button and set up its behavior.
        
        Button callEmergencyContact = (Button) findViewById(R.id.callEmergencyContact);
        callEmergencyContact.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			call("emergencyContactNumber");
    		}
    	});

        Button addEmergencyContact = (Button) findViewById(R.id.addEmergencyContact);
        SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        if (settings.getBoolean("addedEmergencyContact", false)) {
        	TextView tv = (TextView) findViewById(R.id.addEmergencyContact);
    		tv.setText("Edit Emergency Contact");
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.edit_emergencyboy, 0, 0, 0);
        }
        addEmergencyContact.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
        		Intent addEmergencyContactIntent = new Intent(view.getContext(), AddEmergencyContactActivity.class);
        		startActivity(addEmergencyContactIntent);      		
    		}
    	});
        
        Button callDoctorContact = (Button) findViewById(R.id.callDoctorContact);
        callDoctorContact.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
    			call("doctorContactNumber");
    		}
    	});
       
        
        Button addDoctorContact = (Button) findViewById(R.id.addDoctorContact);
        if (settings.getBoolean("addedDoctorContact", false)) {
        	TextView tv = (TextView) findViewById(R.id.addDoctorContact);
    		tv.setText("Edit Doctor Contact");
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.edit_doctorboy, 0, 0, 0);
        }
        addDoctorContact.setOnClickListener(new View.OnClickListener() {
    		public void onClick(View view) {
        		Intent addDoctorContactIntent = new Intent(view.getContext(), AddDoctorContactActivity.class);
        		startActivity(addDoctorContactIntent);        		

    		}
    	});

    }
    
    @Override
    protected void onResume() {
    	super.onResume();
        if (!resumeHasRun) {
            resumeHasRun = true;
            return;
        }
    	
    	SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        if (settings.getBoolean("addedEmergencyContact", false)) {
        	TextView tv = (TextView) findViewById(R.id.addEmergencyContact);
    		tv.setText("Edit Emergency Contact");
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.edit_emergencyboy, 0, 0, 0);
        }
        if (settings.getBoolean("addedDoctorContact", false)) {
        	TextView tv = (TextView) findViewById(R.id.addDoctorContact);
    		tv.setText("Edit Doctor Contact");
    		tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.edit_doctorboy, 0, 0, 0);
        }
    }
    
    private void call(String number) {
    	try {
    		Intent callIntent = new Intent(Intent.ACTION_CALL);
    		SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
    		String telephoneNumber = settings.getString(number, null);
    		if (telephoneNumber == null){
    			createToastIfBadNumber();
    			return;
    		}
    		String telNo = "tel:" + telephoneNumber;
    		
    		callIntent.setData(Uri.parse(telNo));
    		startActivity(callIntent);
    	} catch (ActivityNotFoundException e) {
    		Log.e("calling contact", "call failed", e);
    	}
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    private void createToastIfBadNumber(){
		Context context = getApplicationContext();
		CharSequence text = "Sorry, you can't make a call because the number doesn't work.";
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
    } 
    

	
}
