package com.example.asthmamanager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class TroubleBreathingActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trouble_breathing);        
        
        Button logTrouble = (Button) findViewById(R.id.log_trouble_breathing);
        logTrouble.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		String currentLog = getSymptomLogText();
        		Log.d("full text of current log", currentLog);
        		String thisSymptom = getSymptomString();
        		Log.d("text of this symptom", thisSymptom);
        		
        		String toLog = thisSymptom + currentLog;        		
        		writeStringToLog(toLog);
        		
        		finish();
        	}
        });
        
     // Declare each button and set up its behavior.

        
        /*Button _____ = (Button) findViewById(R.id._____);
        ______.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		//does nothing, because there's no _____ page
        	}
        })*/
        
    }

    private String getSymptomLogText() {		
 		StringBuffer fileContent = new StringBuffer("");
 		try {
 			FileInputStream fis = openFileInput("symptom_log.txt");
 			byte[] buffer = new byte[1024];
 			while (fis.read(buffer) != -1) {
 				fileContent.append(new String(buffer));
 			}
 		} catch (FileNotFoundException e1) {
 			e1.printStackTrace();
 		} catch (IOException e) {
 			e.printStackTrace();
 		}
 		return fileContent.toString();
     }
     
     private String getSymptomString() {
     	Calendar c = Calendar.getInstance();
 		int month = c.get(Calendar.MONTH) + 1;
 		int day = c.get(Calendar.DATE);
 		int year = c.get(Calendar.YEAR);
 		int hour = c.get(Calendar.HOUR_OF_DAY);
 		int minute = c.get(Calendar.MINUTE);
 		
 		String symptom = month + "/" + day + "/" + year + " " +
 		  hour + ":" + minute + "; user reported trouble breathing\n";
 		
 		return symptom;
     }
     
     private void writeStringToLog(String toWrite) {
     	FileOutputStream fos;
 		try {
 			fos = openFileOutput("symptom_log.txt", Context.MODE_PRIVATE);
 			fos.write(toWrite.getBytes());
 	    	fos.close();
 		} catch (FileNotFoundException e) {
 			e.printStackTrace();
 		} catch (IOException e) {
 			e.printStackTrace();
 		}
     }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

}
