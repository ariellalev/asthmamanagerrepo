package com.example.asthmamanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddDoctorContactActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_doctor_contact);
        
        Button addNewDoctorContact = (Button) findViewById(R.id.saveNewDC);
        addNewDoctorContact.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        		SharedPreferences.Editor editor = settings.edit();
        		
        		EditText nameField = (EditText) findViewById(R.id.doctor_name);
                EditText numberField = (EditText) findViewById(R.id.doctor_phone_number);
                EditText emailField = (EditText) findViewById(R.id.doctor_email_address);
                
                String name = nameField.getText().toString();
                String number = numberField.getText().toString();
                String email = emailField.getText().toString();
                                
                if (!isUserInputValid(name, number, email)){
                	return;
                }
                
                editor.putString("doctorContactName", name);
                editor.putString("doctorContactNumber", number);
                editor.putString("doctorEmailAddress", email);
                editor.putBoolean("addedDoctorContact", true);
                
                editor.commit();
                finish();
        	}
        });

     // Declare each button and set up its behavior.
        

        //uncomment the following code and edit it if you want more working buttons on this page
        
        /*Button _____ = (Button) findViewById(R.id._____);
        ______.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		//does nothing, because there's no _____ page
        	}
        })*/
        SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        try
        {
        	EditText nameField = (EditText) findViewById(R.id.doctor_name);
        	nameField.setText(settings.getString("doctorContactName", null));
        }
        catch (NullPointerException e){
        	
        }
        try
        {
        	EditText numberField = (EditText) findViewById(R.id.doctor_phone_number);
        	numberField.setText(settings.getString("doctorContactNumber", null));
        }
        catch (NullPointerException e){
        	
        }
        try 
        {
        	EditText emailField = (EditText) findViewById(R.id.doctor_email_address);   
        	emailField.setText(settings.getString("doctorEmailAddress", null));
        }
        catch (NullPointerException e){
        	
        }
    }

    private boolean isUserInputValid(String name, String number,
			String email) {
        boolean isValidName = name.matches("[a-zA-Z ]+");
        boolean isValidNumber = number.matches("[(]{1}[0-9]{3}[)]{1} [0-9]{3}[-]{1}[0-9]{4}");
        boolean isValidEmailAddress = email.matches("[a-zA-Z0-9._%+-]" + "*@[a-z0-9.]" + "*[a-z0-9]");

        if(!isValidName || !isValidNumber || !isValidEmailAddress){
        	if (!isValidName){
        		//have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the name you entered contains characters other than letters. \nPlease try again.";
        		createToast(text);
        	}
        	if(!isValidNumber){                		
        		//	have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the number you entered does not match the intended formatting. \nPlease try again.";
        		createToast(text);
        	}
        	if(!isValidEmailAddress){
        		//	have a pop up notification that the string doesn't match
        		CharSequence text = "Sorry, the email address you entered is not a valid email address. \nPlease try again.";
        		createToast(text);
        	}
    		finish();
    		return false;
        }  
        return true;
		
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    private void createToast(CharSequence text){
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
    } 
}
