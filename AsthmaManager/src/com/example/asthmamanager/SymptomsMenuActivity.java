package com.example.asthmamanager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SymptomsMenuActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.symptoms_menu_layout);
        
        
     // Declare each button and set up its behavior.
        Button symptoms_log = (Button) findViewById(R.id.symptoms_log);
        symptoms_log.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent symptomLogIntent = new Intent(view.getContext(), SymptomLogActivity.class);
        		startActivity(symptomLogIntent);
        	}
        });
        
        Button coughing = (Button) findViewById(R.id.coughing);
        coughing.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent coughingIntent = new Intent(view.getContext(), CoughingActivity.class);
        		startActivity(coughingIntent);
        	}
        });
        
        Button wheezing = (Button) findViewById(R.id.wheezing);
        wheezing.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent wheezingIntent = new Intent(view.getContext(), WheezingActivity.class);
        		startActivity(wheezingIntent);
        	}
        });
        
        Button trouble_breathing = (Button) findViewById(R.id.trouble_breathing);
        trouble_breathing.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent troubleBreathingIntent = new Intent(view.getContext(), TroubleBreathingActivity.class);
        		startActivity(troubleBreathingIntent);
        	}
        });
        
        
        Button shortness_of_breath = (Button) findViewById(R.id.shortness_of_breath);
        shortness_of_breath.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		Intent shortnessOfBreathIntent = new Intent(view.getContext(), ShortnessOfBreathActivity.class);
        		startActivity(shortnessOfBreathIntent);
        	}
        });
        
        /*Button _____ = (Button) findViewById(R.id._____);
        ______.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		//does nothing, because there's no _____ page
        	}
        })*/
        
        Button email = (Button) findViewById(R.id.email);
        email.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	System.out.println("Made it here 4");
            	doSendFile();
            }
        });

        /*Button _____ = (Button) findViewById(R.id._____);
    	______.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                    //does nothing, because there's no _____ page
            }
    })*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//	Inflate the menu; this adds items to the action bar if it is present.
    	getMenuInflater().inflate(R.menu.activity_main, menu);
    	return true;
    }

    private String getSymptomLogText() {                
        StringBuffer fileContent = new StringBuffer("");
        try {
        	FileInputStream fis = openFileInput("symptom_log.txt");
            byte[] buffer = new byte[1024];
            while (fis.read(buffer) != -1) {
            	fileContent.append(new String(buffer));
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContent.toString();
    }

    public void doSendFile() {
    	String fileName = "/sdcard/myDir/symptom_log.txt"; 
    	SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
		String emailAddress = settings.getString("doctorEmailAddress", null) + ", " + 
				settings.getString("emergencyContactEmail", null);
    	Intent i = new Intent(Intent.ACTION_SEND);
    	i.setType("text/plain");
    	i.putExtra(Intent.EXTRA_EMAIL, new String[] { emailAddress });
    	i.putExtra(Intent.EXTRA_SUBJECT, "Symptom Log");
    	i.putExtra(Intent.EXTRA_TEXT, getSymptomLogText());
    	try {
    		startActivity(Intent.createChooser(i, "Send mail..."));
    	} catch (android.content.ActivityNotFoundException ex) {
    		Toast.makeText(getBaseContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
    	}
    	String toLog = "";
    	writeStringToLog(toLog);

    }
    
    private void writeStringToLog(String toWrite) {
     	FileOutputStream fos;
 		try {
 			fos = openFileOutput("symptom_log.txt", Context.MODE_PRIVATE);
 			fos.write(toWrite.getBytes());
 	    	fos.close();
 		} catch (FileNotFoundException e) {
 			e.printStackTrace();
 		} catch (IOException e) {
 			e.printStackTrace();
 		}
     }

    
}
