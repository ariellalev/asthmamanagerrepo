package com.example.asthmamanager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.os.Bundle;
import android.widget.TextView;

public class SymptomLogActivity extends AbstractActivity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.symptom_log_layout);
		
		StringBuffer fileContent = new StringBuffer("");
		try {
			FileInputStream fis = openFileInput("symptom_log.txt");
			byte[] buffer = new byte[1024];
			while (fis.read(buffer) != -1) {
				fileContent.append(new String(buffer));
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		TextView tv = (TextView) findViewById(R.id.symptom_log);
		tv.setText(fileContent.toString());
		
	}
	
}
