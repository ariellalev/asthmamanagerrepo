package com.example.asthmamanager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


//EDIT DOCTOR WILL CRASH IF THE DOCTOR DOESN'T EXIST
public class EditDoctorContactActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_doctor_contact);
        
        Button editDC = (Button) findViewById(R.id.saveDCEdit);
        editDC.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        		SharedPreferences.Editor editor = settings.edit();
        		
        		EditText nameField = (EditText) findViewById(R.id.doctor_name);
                EditText numberField = (EditText) findViewById(R.id.doctor_phone_number);
                EditText emailField = (EditText) findViewById(R.id.doctor_email_address);
                
                String name = nameField.getText().toString();
                editor.putString("doctorContactName", name);
                String number = numberField.getText().toString();
                editor.putString("doctorContactNumber", number);
                String email = emailField.getText().toString();
                editor.putString("doctorEmailAddress", email);
                
                editor.commit();
                finish();
        	}
        	
        	
        });

     // Declare each button and set up its behavior.

        //uncomment the following code and edit it if you want more working buttons on this page
        
        /*Button _____ = (Button) findViewById(R.id._____);
        ______.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		//does nothing, because there's no _____ page
        	}
        })*/
        
        SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        EditText nameField = (EditText) findViewById(R.id.doctor_name);
        EditText numberField = (EditText) findViewById(R.id.doctor_phone_number);
        EditText emailField = (EditText) findViewById(R.id.doctor_email_address);
        
        nameField.setText(settings.getString("doctorContactName", null));
        numberField.setText(settings.getString("doctorContactNumber", null));
        emailField.setText(settings.getString("doctorEmailAddress", null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    

	
}
