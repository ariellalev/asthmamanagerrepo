package com.example.asthmamanager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditContactActivity extends AbstractActivity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_contact);
        
        Button editEC = (Button) findViewById(R.id.saveECEdit);
        editEC.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        		SharedPreferences.Editor editor = settings.edit();
        		
        		EditText nameField = (EditText) findViewById(R.id.name);
                EditText numberField = (EditText) findViewById(R.id.phone_number);
                EditText emailField = (EditText) findViewById(R.id.email_address);
                		
                String name = nameField.getText().toString();
                editor.putString("emergencyContactName", name);
                String number = numberField.getText().toString();
                editor.putString("emergencyContactNumber", number);
                String email = emailField.getText().toString();
                editor.putString("emergencyContactEmail", email);
                editor.putBoolean("addedEmergencyContact", true);
                
                editor.commit();
                finish();
        	}
        	
        	
        });

     // Declare each button and set up its behavior.

        //uncomment the following code and edit it if you want more working buttons on this page
        
        /*Button _____ = (Button) findViewById(R.id._____);
        ______.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		//does nothing, because there's no _____ page
        	}
        })*/
        
        SharedPreferences settings = getSharedPreferences("GLOBAL_SETTINGS", 0);
        EditText nameField = (EditText) findViewById(R.id.name);
        EditText numberField = (EditText) findViewById(R.id.phone_number);
        EditText emailField = (EditText) findViewById(R.id.email_address);
        
        nameField.setText(settings.getString("emergencyContactName", null));
        numberField.setText(settings.getString("emergencyContactNumber", null));
        emailField.setText(settings.getString("emergencyContactEmail", null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    

	
}
